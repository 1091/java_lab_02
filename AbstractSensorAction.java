package ru.novsu.java.labs.actions;

import ru.novsu.java.labs.base.Action;

public abstract class AbstractSensorAction implements Action {
	protected abstract Boolean isRegistered();
	
	public final void run(){
		while (!isRegistered()) {}
	}
}
