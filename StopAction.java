package ru.novsu.java.labs.actions;

import lejos.nxt.Motor;
import ru.novsu.java.labs.base.*;

public class StopAction implements Action{
	public void run(){
		Motor.A.stop(true);
		Motor.B.stop();
	}

	@Override
	public Boolean isOk() {
		return true;
	}	
}
