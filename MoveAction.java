package ru.novsu.java.labs.actions;

import lejos.nxt.Motor;
import ru.novsu.java.labs.base.*;

public class MoveAction implements Action{
	public void run(){
		Motor.A.setSpeed(100);
		Motor.B.setSpeed(100);
		Motor.A.forward();
		Motor.B.forward();
	}
	
	public Boolean isOk() {
		return true;
	}
}


