package ru.novsu.java.labs.actions;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;


public class BumperReleasedAction extends AbstractSensorAction {

	public BumperReleasedAction(){
		sensor = new TouchSensor(SensorPort.S1);
	}
	
	@Override
	protected Boolean isRegistered() {
		if (sensor.isPressed()){
			return true;
		} else {
			return false;
		}		
	}
	
	public Boolean isOk() {
		return true;
	}
	
	private TouchSensor sensor;
}
