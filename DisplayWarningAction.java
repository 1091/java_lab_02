package ru.novsu.java.labs.actions;

import lejos.nxt.LCD;
import lejos.nxt.Sound;
import ru.novsu.java.labs.base.*;


public class DisplayWarningAction implements Action {
	public void run() {
		LCD.drawString("HETROWPUI", 4, 4);
		Sound.beep();
	}
	
	public Boolean isOk() {
		return true;
	}
}
