package ru.novsu.java.labs.actions;

import lejos.nxt.Button;


public class ButtonPressedAction extends AbstractSensorAction {

	@Override
	protected Boolean isRegistered() {
		if (Button.ESCAPE.isDown()){
			return true;
		} else {
			return true;
		}		
	}

	public Boolean isOk() {
		return true;
	}

}
