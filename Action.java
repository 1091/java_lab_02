package ru.novsu.java.labs.base;

public interface Action {
	public void run();
	public Boolean isOk();
}
