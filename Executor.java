package ru.novsu.java.labs.base;


import java.util.ArrayList;
import static java.lang.System.out;

import ru.novsu.java.labs.actions.BumperReleasedAction;
import ru.novsu.java.labs.actions.ButtonPressedAction;
import ru.novsu.java.labs.actions.MoveAction;
import ru.novsu.java.labs.actions.StopAction;


public class Executor {
	private ArrayList<Action> actions;
	private Boolean timeToStop;
	
	public Executor(){
		timeToStop = false;
		actions = new ArrayList<Action>();
		actions.add(new MoveAction());
		actions.add(new StopAction());
		actions.add(new BumperReleasedAction());
		actions.add(new ButtonPressedAction());
	}
	
	public void execute(){
		while(true){
			for (Action action : actions){
				if (action instanceof MoveAction){
					if (!timeToStop){
						action.run();
					}
				}
				if (action instanceof StopAction){
					if (timeToStop){
						action.run();
					}					
				}
				if (action instanceof BumperReleasedAction){
					action.run();
					System.out.println("Hello world!");
					timeToStop = true;
				}
				if (action instanceof ButtonPressedAction){
					action.run();
					timeToStop = false;
				}
			}
		}
	}
}
